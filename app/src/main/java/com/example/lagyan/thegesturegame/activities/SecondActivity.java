package com.example.lagyan.thegesturegame.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lagyan.thegesturegame.util.CircleGridAdapter;
import com.example.lagyan.thegesturegame.R;
import com.example.lagyan.thegesturegame.util.Constants;

import java.util.Arrays;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener, Constants {

    TextView displayStatus;
    Button thirdLevel;
    RelativeLayout layoutBelow;
    View circle;
    GradientDrawable shape;
    GridView circleGridView;
    CircleGridAdapter adapter;
    float xCenter, yCenter, radius;
    boolean[] flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        displayStatus = (TextView) findViewById(R.id.messageSecond);
        layoutBelow = (RelativeLayout) findViewById(R.id.relativelayout4);
        thirdLevel = (Button) findViewById(R.id.thirdlevel);
        thirdLevel.setOnClickListener(this);

        circleGridView = (GridView) findViewById(R.id.circles_grid);
        circleGridView.setNumColumns(NUM_OF_COLS);

        flag = new boolean[NUM_OF_COLS * NUM_OF_ROWS];
        Arrays.fill(flag, false);

        adapter = new CircleGridAdapter(SecondActivity.this, NUM_OF_ROWS, NUM_OF_COLS, GRIDVIEW_WIDTH, GRIDVIEW_HEIGHT);
        circleGridView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        circleGridView.setOnTouchListener(SecondActivity.this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, ThirdActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int position = circleGridView.pointToPosition((int) event.getX(), (int) event.getY());
        if (position < 0) {
            displayStatus.setText(OUTSIDE_CIRCLE_TEXT);
            return true;
        }
        getCenterCoordinates(position);
        if (event.getAction() == MotionEvent.ACTION_MOVE || event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_UP) {
            double distance = calculateDistance(event.getX(), event.getY());
            if (distance < radius) {
                toggleColor(INSIDE, position);
            } else if (distance == radius) {
                toggleColor(ON_BOUNDARY, position);
            } else {
                toggleColor(OUTSIDE, position);
            }
        }
        return true;
    }

    public void getCenterCoordinates(int position) {
        circle = circleGridView.getChildAt(position);
        shape = (GradientDrawable) circle.getBackground();
        radius = (circle.getWidth()) / 2;
        xCenter = (circle.getRight() + circle.getLeft()) / 2;
        yCenter = (circle.getBottom() + circle.getTop()) / 2;
    }

    public double calculateDistance(double x, double y) {
        double distance;
        distance = Math.sqrt(Math.pow((x - xCenter), 2) + Math.pow((y - yCenter), 2));
        return distance;
    }

    public void toggleColor(int caseType, int position) {
        switch (caseType) {
            case INSIDE:
                if (!displayStatus.getText().toString().equalsIgnoreCase(INSIDE_CIRCLE_TEXT + position)) {
                    displayStatus.setText(INSIDE_CIRCLE_TEXT + String.valueOf(position));
                    flag[position] = !flag[position];
                }
                break;
            case ON_BOUNDARY:
                shape.setColor(Color.RED);
                displayStatus.setText(ON_BOUNDARY_TEXT + String.valueOf(position));
                break;
            case OUTSIDE:
                displayStatus.setText(OUTSIDE_CIRCLE_TEXT + String.valueOf(position));
                break;
        }
        if (flag[position]) {
            shape.setColor(Color.rgb(176, 229, 124));
        } else {
            shape.setColor(Color.WHITE);

        }
    }
}
