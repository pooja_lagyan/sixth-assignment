package com.example.lagyan.thegesturegame.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lagyan.thegesturegame.R;
import com.example.lagyan.thegesturegame.util.Constants;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener, Constants {

    TextView displayStatus;
    Button secondLevel;
    RelativeLayout layoutBelow;
    View circle;
    GradientDrawable shape;
    float xCenter, yCenter, radius;
    int insideGoCount = 0;
    boolean flag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        displayStatus = (TextView) findViewById(R.id.message);
        secondLevel = (Button) findViewById(R.id.secondlevel);
        layoutBelow = (RelativeLayout) findViewById(R.id.relativelayout2);

        circle = findViewById(R.id.circle);
        shape = (GradientDrawable) circle.getBackground();

        layoutBelow.setOnTouchListener(this);
        secondLevel.setOnClickListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        getCenterCoordinates();
        double distance;
        if (event.getAction() == MotionEvent.ACTION_MOVE || event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_UP) {
            distance = calculateDistance(event.getX(), event.getY());
            if (distance < radius) {
                toggleColor(INSIDE);
            } else if (distance == radius) {
                toggleColor(ON_BOUNDARY);
            } else {
                toggleColor(OUTSIDE);
            }
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
        finish();
    }

    public void getCenterCoordinates() {
        radius = (circle.getBottom() + circle.getTop()) / 2;
        xCenter = (circle.getRight() + circle.getLeft()) / 2;
        yCenter = (circle.getBottom() + circle.getTop()) / 2;
    }

    public double calculateDistance(double x, double y) {
        double distance = Math.sqrt(Math.pow((x - xCenter), 2) + Math.pow((y - yCenter), 2));
        return distance;
    }

    public void toggleColor(int pos) {
        switch (pos) {
            case INSIDE:
                if (!displayStatus.getText().toString().equalsIgnoreCase(INSIDE_CIRCLE_TEXT)) {
                    displayStatus.setText(INSIDE_CIRCLE_TEXT);
                    flag = !flag;
                }
                break;
            case ON_BOUNDARY:
                shape.setColor(Color.RED);
                displayStatus.setText(ON_BOUNDARY_TEXT);
                break;
            case OUTSIDE:
                displayStatus.setText(OUTSIDE_CIRCLE_TEXT);
                break;
        }
        if (flag) {
            shape.setColor(Color.rgb(176, 229, 124));
        } else {
            shape.setColor(Color.WHITE);

        }
    }
}
