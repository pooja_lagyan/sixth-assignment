package com.example.lagyan.thegesturegame.util;

/**
 * Created by LAGYAN on 10/12/2015.
 */
public interface Constants {
    String INSIDE_CIRCLE_TEXT = "Inside The Circle ";
    String OUTSIDE_CIRCLE_TEXT = "Outside The Circle ";
    String ON_BOUNDARY_TEXT = "On the Boundary of Circle ";
    String ERROR_KEY = "Enter rows,cols <= 5";
    String COMPLETE_DATA_KEY = "Enter the complete data";
    int NUM_OF_ROWS = 3;
    int NUM_OF_COLS = 3;
    int INSIDE = 1;
    int ON_BOUNDARY = 2;
    int OUTSIDE = 3;
    float GRIDVIEW_HEIGHT = 550;
    float GRIDVIEW_WIDTH = 750;
}
