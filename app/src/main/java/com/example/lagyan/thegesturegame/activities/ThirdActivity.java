package com.example.lagyan.thegesturegame.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lagyan.thegesturegame.util.CircleGridAdapter;
import com.example.lagyan.thegesturegame.R;
import com.example.lagyan.thegesturegame.util.Constants;

import java.util.Arrays;

public class ThirdActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener, Constants {

    TextView displayStatus;
    EditText rows, columns;
    Button firstLevel, displayCircle;
    LinearLayout layoutBelow;
    GradientDrawable shape;
    int numOfRows, numOfCols;
    GridView circleGridView;
    CircleGridAdapter adapter;
    View circle, previousCircle = null;
    float xCenter, yCenter, radius;
    boolean[] flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        displayStatus = (TextView) findViewById(R.id.messageThird);
        rows = (EditText) findViewById(R.id.numRows);
        columns = (EditText) findViewById(R.id.numCols);

        firstLevel = (Button) findViewById(R.id.firstLevel);
        displayCircle = (Button) findViewById(R.id.displayCircles);
        circleGridView = (GridView) findViewById(R.id.circles_grid2);

        layoutBelow = (LinearLayout) findViewById(R.id.relativelayout6);

        firstLevel.setOnClickListener(this);
        displayCircle.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.displayCircles:
                closeKeyboard();
                if (columns.getText().toString().isEmpty() || rows.getText().toString().isEmpty())
                    Toast.makeText(this, COMPLETE_DATA_KEY, Toast.LENGTH_SHORT).show();
                else {
                    numOfCols = Integer.parseInt(columns.getText().toString());
                    numOfRows = Integer.parseInt(rows.getText().toString());
                    flag = new boolean[numOfCols * numOfRows];
                    Arrays.fill(flag, false);

                    if ((numOfRows > 0 && numOfRows <= 5) && (numOfCols > 0 && numOfCols <= 5)) {
                        float gridViewHeight = circleGridView.getHeight(), gridViewWidth = circleGridView.getWidth();
                        circleGridView.setNumColumns(numOfCols);
                        adapter = new CircleGridAdapter(ThirdActivity.this, numOfRows, numOfCols, gridViewHeight, gridViewWidth);
                        circleGridView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        circleGridView.setOnTouchListener(ThirdActivity.this);
                    } else
                        displayStatus.setText(ERROR_KEY);
                }
                break;

            case R.id.firstLevel:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int position = circleGridView.pointToPosition((int) event.getX(), (int) event.getY());
        if (position < 0) {
            displayStatus.setText(OUTSIDE_CIRCLE_TEXT);
            return true;
        }
        getCenterCoordinates(position);
        if (event.getAction() == MotionEvent.ACTION_MOVE || event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_UP) {
            double distance = calculateDistance(event.getX(), event.getY());
            if (distance < radius) {
                toggleColor(INSIDE, position);
            } else if (distance == radius) {
                toggleColor(ON_BOUNDARY, position);
            } else {
                toggleColor(OUTSIDE, position);
            }
        }
        previousCircle = circle;
        return true;
    }

    public void getCenterCoordinates(int position) {
        circle = circleGridView.getChildAt(position);
        shape = (GradientDrawable) circle.getBackground();
        radius = (circle.getWidth()) / 2;
        xCenter = (circle.getRight() + circle.getLeft()) / 2;
        yCenter = (circle.getBottom() + circle.getTop()) / 2;
    }

    public double calculateDistance(double x, double y) {
        double distance;
        distance = Math.sqrt(Math.pow((x - xCenter), 2) + Math.pow((y - yCenter), 2));
        return distance;
    }

    public void toggleColor(int pos, int position) {
        switch (pos) {
            case INSIDE:
                if (!displayStatus.getText().toString().equalsIgnoreCase(INSIDE_CIRCLE_TEXT + position)) {
                    displayStatus.setText(INSIDE_CIRCLE_TEXT + String.valueOf(position));
                    flag[position] = !flag[position];
                }
                break;
            case ON_BOUNDARY:
                shape.setColor(Color.RED);
                displayStatus.setText(ON_BOUNDARY_TEXT + String.valueOf(position));
                break;
            case OUTSIDE:
                displayStatus.setText(OUTSIDE_CIRCLE_TEXT + String.valueOf(position));
                break;
        }
        if (flag[position]) {
            shape.setColor(Color.rgb(176, 229, 124));
        } else {
            shape.setColor(Color.WHITE);

        }
    }

    // Close the keyboard on button click
    void closeKeyboard() {
        View view = this.getCurrentFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
