package com.example.lagyan.thegesturegame.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;

import com.example.lagyan.thegesturegame.R;

/**
 * Created by LAGYAN on 10/12/2015.
 */
public class CircleGridAdapter extends BaseAdapter {
    Context context;
    int numOfRows, numOfCols, width, height;

    public CircleGridAdapter(Context context, int numOfRows, int numOfCols, float height, float width) {
        this.context = context;
        this.numOfRows = numOfRows;
        this.numOfCols = numOfCols;
        this.width = (int) (width / numOfCols);
        this.height = (int) (height / numOfRows);
    }

    @Override
    public int getCount() {
        return numOfCols * numOfRows;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (width < height)
            height = width;
        else if (height < width)
            width = height;
        AbsListView.LayoutParams params = new AbsListView.LayoutParams(width, height);
        LayoutInflater li = LayoutInflater.from(context);
        convertView = li.inflate(R.layout.grid_circle_layout, parent, false);
        convertView.setLayoutParams(params);
        return convertView;
    }
}
